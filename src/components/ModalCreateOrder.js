import { Box, Button, Divider, Grid, MenuItem, Modal, Select, TextField, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { Form } from "reactstrap";

const ModalCreateOrder = ({ openModal, closeModal, snackbarOpen, snackbarClose, snackbarSeverity, snackbarMess }) => {
    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: "90%",
        bgcolor: 'background.paper',
        border: '2px solid #000',
        boxShadow: 24,
        p: 4,
    };
    const [diaChi, setDiaChi] = useState("");
    const [duongKinh, setDuongKinh] = useState("");
    const [email, setEmail] = useState("");
    const [hoTen, setHoTen] = useState("");
    const [idLoaiNuocUong, setIdLoaiNuocUong] = useState("none");
    const [idVourcher, setIdVourcher] = useState("");
    const [loaiPizza, setLoaiPizza] = useState("none");
    const [salad, setSalad] = useState("");
    const [soDienThoai, setSoDienThoai] = useState("");
    const [soLuongNuoc, setSoLuongNuoc] = useState("");
    const [suon, setSuon] = useState("");
    const [thanhTien, setThanhTien] = useState(0);
    const [kichCo, setKichCo] = useState("none");
    const [loiNhan, setLoiNhan] = useState("");
    const [giamGia, setGiamGia] = useState(0);

    //Get Drinks list
    const [drinksData, setDrinksData] = useState([])
    const getDrinkList = async () => {
        const response = await fetch("http://42.115.221.44:8080/devcamp-pizza365/drinks");
        const data = await response.json();
        return data;
    }
    //get Voucher by Id
    const getVoucherByVoucherId = async (voucher) => {
        const response = await fetch("http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/" + voucher);
        const data = await response.json();
        return data;
    }
    //creat order api
    const creatOrderApi = async (orderData) => {
        let body = {
            method: 'POST',
            body: JSON.stringify(orderData),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            }
        };
        const response = await fetch("http://42.115.221.44:8080/devcamp-pizza365/orders", body);
        const data = await response.json();
        return data;
    }

    //Hàm xử lý sự kiện thay đổi Combo Size(các thông tin đường kính, sườn, salad, số lượng nước uống, thành tiền sẽ tự động được điền)
    const onChangeComboSizeHandle = (value) => {
        switch (value) {
            case "S":
                setKichCo("S");
                setDuongKinh("20");
                setSuon("2");
                setSalad("200");
                setSoLuongNuoc("2");
                setThanhTien(150000);
                break;
            case "M":
                setKichCo("M");
                setDuongKinh("25");
                setSuon("4");
                setSalad("300");
                setSoLuongNuoc("3");
                setThanhTien(200000);
                break;
            case "L":
                setKichCo("L");
                setDuongKinh("30");
                setSuon("8");
                setSalad("500");
                setSoLuongNuoc("4");
                setThanhTien(250000);
                break;
            default:
                setKichCo("");
                setDuongKinh("");
                setSuon("");
                setSalad("");
                setSoLuongNuoc("");
                setThanhTien(0);
                break;
        }
    }

    //Hàm xử lý sự kiện nhập voucher ID, lấy phần trăm giảm giá
    const onChangeVoucherIdHandle = (voucher) => {
        setIdVourcher(voucher);
        if (voucher !== "") {
            getVoucherByVoucherId(voucher)
                .then((data) => {
                    setGiamGia(data.phanTramGiamGia);
                    snackbarClose();
                })
                .catch((error) => {
                    snackbarSeverity("error");
                    snackbarMess("Không tìm thấy mã giảm giá!");
                    snackbarOpen();
                    setGiamGia(0);
                });
        }
        else setGiamGia(0);

    }

    //Hàm xử lý sự kiện khi click creat order
    const onSubmitHandle = (event) => {
        let customerInfo = {
            diaChi: diaChi,
            duongKinh: duongKinh,
            email: email,
            hoTen: hoTen,
            idLoaiNuocUong: idLoaiNuocUong,
            idVourcher: idVourcher,
            kichCo: kichCo,
            loaiPizza: loaiPizza,
            loiNhan: loiNhan,
            salad: salad,
            soDienThoai: soDienThoai,
            soLuongNuoc: soLuongNuoc,
            suon: suon,
            thanhTien: thanhTien,
            giamGia: giamGia
        }
        event.preventDefault();
        let isValidated = validateOrder();
        if (isValidated) {
            creatOrderApi(customerInfo)
                .then((data) => {
                    snackbarSeverity("success");
                    snackbarMess("Đã tạo Order thành công! Mã order của bạn là: " + data.orderId);
                    snackbarOpen();
                    resetModal();
                    closeModal();
                })
                .catch((error) => {
                    snackbarSeverity("error");
                    snackbarMess(error);
                    snackbarOpen();
                })
        }
    }
    //validated dữ liệu
    const validateOrder = () => {
        if (kichCo === "" || kichCo === "none") {
            snackbarSeverity("error");
            snackbarMess("Hãy chọn cỡ combo!");
            snackbarOpen();
            return false;
        }
        if (idLoaiNuocUong === "" || idLoaiNuocUong === "none") {
            snackbarSeverity("error");
            snackbarMess("Hãy chọn loại đồ uống!");
            snackbarOpen();
            return false;
        }
        if (loaiPizza === "" || loaiPizza === "none") {
            snackbarSeverity("error");
            snackbarMess("Hãy chọn loại pizza!");
            snackbarOpen();
            return false;
        }
        return true;
    }
    //reset modal
    const resetModal = () => {
        setDiaChi("");
        setDuongKinh("");
        setEmail("");
        setHoTen("");
        setIdLoaiNuocUong("none");
        setIdVourcher("");
        setLoaiPizza("none");
        setSalad("");
        setSoDienThoai("");
        setSoLuongNuoc("");
        setSuon("");
        setThanhTien(0);
        setKichCo("none");
        setLoiNhan("");
        setGiamGia(0);
    }

    useEffect(() => {
        getDrinkList()
            .then((data) => {
                setDrinksData(data);
            })
            .catch((error) => {
                throw error
            });
    }, [])

    return (
        <Modal open={openModal} id="modal-insert-user" aria-labelledby="modal-modal-title" aria-describedby="modal-modal-description" >
            <Box sx={style}>
                <Typography id="modal-modal-title" variant="h4" component="h2" textAlign="center" marginBottom={2}>
                    Create Order
                </Typography>
                <Divider />
                <Form onSubmit={onSubmitHandle}>
                    <Grid container spacing={1} marginTop={3}>
                        <Grid item xs={4}>
                            <Grid container spacing={2} marginBottom={3}>
                                <Grid item xs={4}>
                                    <label>Họ và tên:</label>
                                </Grid>
                                <Grid item xs={8}>
                                    <TextField variant="outlined" value={hoTen} onChange={(event) => { setHoTen(event.target.value) }} required />
                                </Grid>
                            </Grid>
                            <Grid container spacing={2} marginBottom={3}>
                                <Grid item xs={4}>
                                    <label>Địa chỉ:</label>
                                </Grid>
                                <Grid item xs={8}>
                                    <TextField variant="outlined" value={diaChi} onChange={(event) => { setDiaChi(event.target.value) }} required />
                                </Grid>
                            </Grid>
                            <Grid container spacing={2} marginBottom={3}>
                                <Grid item xs={4}>
                                    <label>Số điện thoại:</label>
                                </Grid>
                                <Grid item xs={8}>
                                    <TextField type="number" variant="outlined" value={soDienThoai} onChange={(event) => { setSoDienThoai(event.target.value) }} required />
                                </Grid>
                            </Grid>
                            <Grid container spacing={2} marginBottom={3}>
                                <Grid item xs={4}>
                                    <label>Email:</label>
                                </Grid>
                                <Grid item xs={8}>
                                    <TextField type="email" variant="outlined" value={email} onChange={(event) => { setEmail(event.target.value) }} />
                                </Grid>
                            </Grid>
                            <Grid container spacing={2} marginBottom={3}>
                                <Grid item xs={4}>
                                    <label>Lời nhắn:</label>
                                </Grid>
                                <Grid item xs={8}>
                                    <TextField variant="outlined" multiline value={loiNhan} onChange={(event) => { setLoiNhan(event.target.value) }} />
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xs={8}>
                            <Grid container spacing={1}>
                                <Grid item xs={6}>
                                    <Grid container spacing={2} marginBottom={3}>
                                        <Grid item xs={6}>
                                            <label>Cỡ combo:</label>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Select fullWidth value={kichCo} onChange={(event) => { onChangeComboSizeHandle(event.target.value) }}>
                                                <MenuItem value="none">Select Pizza size</MenuItem>
                                                <MenuItem value="S">S</MenuItem>
                                                <MenuItem value="M">M</MenuItem>
                                                <MenuItem value="L">L</MenuItem>
                                            </Select>
                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={2} marginBottom={3}>
                                        <Grid item xs={6}>
                                            <label>Sườn nướng (miếng):</label>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <TextField variant="outlined" value={suon} disabled />
                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={2} marginBottom={3}>
                                        <Grid item xs={6}>
                                            <label>Loại Đồ uống:</label>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Select fullWidth value={idLoaiNuocUong} onChange={(event) => setIdLoaiNuocUong(event.target.value)}>
                                                <MenuItem value="none">Select Drink</MenuItem>
                                                {drinksData.map((drink, index) => {
                                                    return (
                                                        <MenuItem key={index} value={drink.maNuocUong}>{drink.tenNuocUong}</MenuItem>
                                                    )
                                                })}
                                            </Select>
                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={2} marginBottom={3}>
                                        <Grid item xs={6}>
                                            <label>Loại pizza:</label>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Select fullWidth value={loaiPizza} onChange={(event) => setLoaiPizza(event.target.value)}>
                                                <MenuItem value="none">Select Pizza Type</MenuItem>
                                                <MenuItem value="Seafood">Hải sản</MenuItem>
                                                <MenuItem value="Hawaii">Hawaii</MenuItem>
                                                <MenuItem value="Bacon">Thịt hun khói</MenuItem>
                                            </Select>
                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={2} marginBottom={3}>
                                        <Grid item xs={6}>
                                            <label>Mã voucher:</label>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <TextField type="number" variant="outlined" value={idVourcher} onChange={(event) => { onChangeVoucherIdHandle(event.target.value) }} />
                                        </Grid>
                                    </Grid>
                                </Grid>
                                <Grid item xs={6}>
                                    <Grid container spacing={2} marginBottom={3}>
                                        <Grid item xs={6}>
                                            <label>Đường kính pizza:</label>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <TextField variant="outlined" value={duongKinh} disabled />
                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={2} marginBottom={3}>
                                        <Grid item xs={6}>
                                            <label>Salad (g):</label>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <TextField variant="outlined" value={salad} disabled />
                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={2} marginBottom={3}>
                                        <Grid item xs={6}>
                                            <label>Số lượng đồ uống:</label>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <TextField variant="outlined" value={soLuongNuoc} disabled />
                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={2} marginBottom={3}>
                                        <Grid item xs={6}>
                                            <label>Giá combo:</label>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <TextField variant="outlined" value={thanhTien.toLocaleString('vi-VN', { style: 'currency', currency: 'VND' })} disabled />
                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={2} marginBottom={3}>
                                        <Grid item xs={6}>
                                            <label>Giảm giá (Discount)%:</label>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <TextField variant="outlined" value={giamGia} disabled />
                                        </Grid>
                                    </Grid>
                                    <Divider />
                                </Grid>
                            </Grid>
                            <Grid container spacing={2} justifyContent="flex-end">
                                <Grid item xs={3}>
                                    <b>Phải thanh toán:</b>
                                </Grid>
                                <Grid item xs={3}>
                                    <TextField variant="outlined" value={(thanhTien - thanhTien * giamGia / 100).toLocaleString('vi-VN', { style: 'currency', currency: 'VND' })} disabled />
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid container spacing={2} marginTop={3} justifyContent="space-around">
                        <Button type="submit" variant="contained" color="success">Create</Button>
                        <Button variant="outlined" onClick={closeModal}>Cancel</Button>
                    </Grid>
                </Form>
            </Box>
        </Modal>
    )
}
export default ModalCreateOrder;