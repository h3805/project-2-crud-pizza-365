import { Button, Container, Grid, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import BasicSnackbar from "./BasicSnackbar";
import ModalCreateOrder from "./ModalCreateOrder";
import ModalDeleteOrder from "./ModalDeleteOrder";
import ModalUpdateOrder from "./ModalUpdateOrder";
import PaginationComponent from "./PaginationComponent";
import SelectNumberPage from "./SelectNumberPage";

const Pizza365Manager = () => {
    //Set page
    const [limit, setLimit] = useState(10);
    const [noPage, setNoPage] = useState(0);
    const [page, setPage] = useState(1);
    const onPageChange = (value) => {
        setPage(value);
    }
    const changeSelectShowPageHandle = (value) => {
        setLimit(value);
    }

    //Get All orders
    const getAllOrders = async () => {
        const response = await fetch("http://42.115.221.44:8080/devcamp-pizza365/orders");
        const data = await response.json();
        return data;
    }

    const [ordersData, setOrdersData] = useState([]);
    const [orderRowData, setOrderRowData] = useState();

    //Snackbar
    const [openSnackbar, setOpenSnackbar] = useState(false);
    const [severitySnackbar, setSeveritySnackbar] = useState("");
    const [messageSnackbar, setMessageSnackbar] = useState("");
    const handleCloseSnackbar = () => setOpenSnackbar(false);
    const handleOpenSnackbar = () => setOpenSnackbar(true);

    //Mở, đóng modal create Order
    const [openModalCreateOrder, setOpenModalCreateOrder] = useState(false);
    const handleOpenModalCreateOrder = () => setOpenModalCreateOrder(true);
    const handleCloseModalCreateOrder = () => setOpenModalCreateOrder(false);

    //Mở, đóng modal update Order
    const [openModalUpdateOrder, setOpenModalUpdateOrder] = useState(false);
    const handleOpenModalUpdateOrder = () => setOpenModalUpdateOrder(true);
    const handleCloseModalUpdateOrder = () => setOpenModalUpdateOrder(false);
    //xử lý sự kiện click button update
    const onClickBtnUpdateHandle = (rowData) => {
        setOrderRowData(rowData);
        handleOpenModalUpdateOrder();
    }

    //Mở, đóng modal delete Order
    const [openModalDeleteOrder, setOpenModalDeleteOrder] = useState(false);
    const handleOpenModalDeleteOrder = () => setOpenModalDeleteOrder(true);
    const handleCloseModalDeleteOrder = () => setOpenModalDeleteOrder(false);
    //xử lý sự kiện click button delete
    const onClickBtnDeleteHandle = (rowData) => {
        setOrderRowData(rowData);
        handleOpenModalDeleteOrder();
    }

    //Use Effect, gọi sau mỗi lần render DOM bao gồm cả lần đầu tiên
    useEffect(() => {
        getAllOrders()
            .then((data) => {
                setOrdersData(data.slice(limit * (page - 1), limit * page));
                setNoPage(Math.ceil(data.length / limit));
            })
            .catch((err) => {
                setSeveritySnackbar("error");
                setMessageSnackbar(err);
                handleOpenSnackbar();
            })
    }, [page, limit, openModalCreateOrder, openModalUpdateOrder, openModalDeleteOrder])

    return (
        <Container>
            <Typography variant="h3" textAlign="center" marginTop={3}>
                PIZZA 365 ORDERS LIST
            </Typography>
            <Grid container marginTop={3} spacing={2}>
                <Grid item xs={6}>
                    <Button variant="contained" color="success" size="large" onClick={handleOpenModalCreateOrder}>Add Order</Button>
                </Grid>
                <Grid item xs={6} textAlign="right">
                    <SelectNumberPage showPage={limit} selectShowPageHandleProp={changeSelectShowPageHandle} />
                </Grid>
            </Grid>
            <Grid container marginTop={3}>
                <Grid item xs={12}>
                    <TableContainer component={Paper}>
                        <Table sx={{ minWidth: 650 }} aria-label="orders table">
                            <TableHead>
                                <TableRow>
                                    <TableCell align="center">Order ID</TableCell>
                                    <TableCell align="center">Kích cỡ combo</TableCell>
                                    <TableCell align="center">Loại Pizza</TableCell>
                                    <TableCell align="center">Nước uống</TableCell>
                                    <TableCell align="center">Thành tiền</TableCell>
                                    <TableCell align="center">Họ và tên</TableCell>
                                    <TableCell align="center">Số điện thoại</TableCell>
                                    <TableCell align="center">Trạng thái</TableCell>
                                    <TableCell align="center">Action</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {ordersData.map((element, index) => {
                                    return (
                                        <TableRow key={index} sx={{ '&:last-child td, &:last-child th': { border: 0 } }} >
                                            <TableCell component="th" scope="row" align="center">{element.orderId}</TableCell>
                                            <TableCell align="center">{element.kichCo}</TableCell>
                                            <TableCell align="center">{element.loaiPizza}</TableCell>
                                            <TableCell align="center">{element.idLoaiNuocUong}</TableCell>
                                            <TableCell align="center">{element.thanhTien}</TableCell>
                                            <TableCell align="center">{element.hoTen}</TableCell>
                                            <TableCell align="center">{element.soDienThoai}</TableCell>
                                            <TableCell align="center">{element.trangThai}</TableCell>
                                            <TableCell align="center">
                                                <Grid container spacing={1}>
                                                    <Grid item xs={6}>
                                                        <Button variant="contained" size="small" onClick={() => onClickBtnUpdateHandle(element)}>Update</Button>
                                                    </Grid>
                                                    <Grid item xs={6}>
                                                        <Button variant="contained" color="error" size="small" onClick={() => onClickBtnDeleteHandle(element)}>Delete</Button>
                                                    </Grid>
                                                </Grid>
                                            </TableCell>
                                        </TableRow>
                                    )
                                })}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>
            </Grid>
            <Grid container marginTop={3} marginBottom={3} justifyContent="flex-end">
                <PaginationComponent countPage={noPage} onChangePageProp={onPageChange} />
            </Grid>
            <BasicSnackbar open={openSnackbar} onClose={handleCloseSnackbar} severity={severitySnackbar} message={messageSnackbar} />
            <ModalCreateOrder openModal={openModalCreateOrder} closeModal={handleCloseModalCreateOrder} snackbarOpen={handleOpenSnackbar} snackbarClose={handleCloseSnackbar} snackbarSeverity={setSeveritySnackbar} snackbarMess={setMessageSnackbar} />
            <ModalUpdateOrder openModal={openModalUpdateOrder} closeModal={handleCloseModalUpdateOrder} snackbarOpen={handleOpenSnackbar} snackbarClose={handleCloseSnackbar} snackbarSeverity={setSeveritySnackbar} snackbarMess={setMessageSnackbar} orderRowData={orderRowData} />
            <ModalDeleteOrder openModal={openModalDeleteOrder} closeModal={handleCloseModalDeleteOrder} orderRowData={orderRowData} snackbarOpen={handleOpenSnackbar} snackbarClose={handleCloseSnackbar} snackbarSeverity={setSeveritySnackbar} snackbarMess={setMessageSnackbar} />
        </Container>
    )
}
export default Pizza365Manager;