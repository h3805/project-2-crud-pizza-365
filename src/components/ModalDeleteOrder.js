import { Box, Button, Divider, Grid, Modal, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { Form } from "reactstrap";

const ModalUpdateOrder = ({ openModal, closeModal, orderRowData, snackbarOpen, snackbarClose, snackbarSeverity, snackbarMess }) => {
    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: "40%",
        bgcolor: 'background.paper',
        border: '2px solid #000',
        boxShadow: 24,
        p: 4,
    };
    const [id, setId] = useState("");

    useEffect(() => {
        if (orderRowData !== undefined) {
            setId(orderRowData.id);
        }
    }, [orderRowData])

    const onSubmitHandle = (event) => {
        event.preventDefault();
        let body = {
            method: 'DELETE'
        };
        fetch("http://42.115.221.44:8080/devcamp-pizza365/orders/" + id, body)
            .then(() => {
                snackbarSeverity("success");
                snackbarMess("Đã Xóa Order thành công!");
                snackbarOpen();
                closeModal();
            })
            .catch((error) => {
                snackbarSeverity("error");
                snackbarMess(error);
                snackbarOpen();
            })
    }

    return (
        <Modal open={openModal} onClose={closeModal} id="modal-delete-user" aria-labelledby="modal-modal-title" aria-describedby="modal-modal-description" >
            <Box sx={style}>
                <Typography id="modal-modal-title" variant="h4" component="h2" textAlign="center" marginBottom={2}>
                    Confirm xóa order!
                </Typography>
                <Divider />
                <Form onSubmit={onSubmitHandle}>
                    <Grid container spacing={1} marginTop={3} marginBottom={3}>
                        <Grid item xs={12}>
                            <Typography variant="h6">Bạn có chắc chắn muốn xóa order này không?</Typography>
                        </Grid>
                    </Grid>
                    <Divider />
                    <Grid container spacing={2} marginTop={3} justifyContent="space-around">
                        <Button type="submit" variant="contained" color="error">Confirm</Button>
                        <Button variant="outlined" onClick={closeModal}>Cancel</Button>
                    </Grid>
                </Form>
            </Box>
        </Modal>
    )
}
export default ModalUpdateOrder;