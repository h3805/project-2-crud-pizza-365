import { Box, Button, Divider, Grid, MenuItem, Modal, Select, TextField, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { Form } from "reactstrap";

const ModalUpdateOrder = ({ openModal, closeModal, orderRowData, snackbarOpen, snackbarClose, snackbarSeverity, snackbarMess }) => {
    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: "90%",
        bgcolor: 'background.paper',
        border: '2px solid #000',
        boxShadow: 24,
        p: 4,
    };
    const [diaChi, setDiaChi] = useState("");
    const [duongKinh, setDuongKinh] = useState("");
    const [email, setEmail] = useState("");
    const [hoTen, setHoTen] = useState("");
    const [idLoaiNuocUong, setIdLoaiNuocUong] = useState("");
    const [idVourcher, setIdVourcher] = useState("");
    const [loaiPizza, setLoaiPizza] = useState("");
    const [salad, setSalad] = useState("");
    const [soDienThoai, setSoDienThoai] = useState("");
    const [soLuongNuoc, setSoLuongNuoc] = useState("");
    const [suon, setSuon] = useState("");
    const [thanhTien, setThanhTien] = useState(0);
    const [kichCo, setKichCo] = useState("");
    const [loiNhan, setLoiNhan] = useState("");
    const [giamGia, setGiamGia] = useState(0);
    const [orderId, setOrderId] = useState("");
    const [trangThai, setTrangThai] = useState("none");
    const [ngayTao, setNgayTao] = useState("");
    const [ngayCapNhat, setNgayCapNhat] = useState("");
    const [id, setId] = useState("");

    const updateOrderApi = async (url, body) => {
        const response = await fetch(url, body);
        const data = await response.json();
        return data;
    }

    useEffect(() => {
        let trangThaiArr = ["open", "cancel", "confirmed"];
        if (orderRowData !== undefined) {
            setDiaChi(orderRowData.diaChi);
            setDuongKinh(orderRowData.duongKinh);
            setEmail(orderRowData.email);
            setHoTen(orderRowData.hoTen);
            setIdLoaiNuocUong(orderRowData.idLoaiNuocUong);
            setIdVourcher(orderRowData.idVourcher);
            setLoaiPizza(orderRowData.loaiPizza);
            setSalad(orderRowData.salad);
            setSoDienThoai(orderRowData.soDienThoai);
            setSoLuongNuoc(orderRowData.soLuongNuoc);
            setSuon(orderRowData.suon);
            setThanhTien(orderRowData.thanhTien);
            setKichCo(orderRowData.kichCo);
            setLoiNhan(orderRowData.loiNhan);
            setGiamGia(orderRowData.giamGia);
            setOrderId(orderRowData.orderId);
            setNgayTao(new Date(orderRowData.ngayTao));
            setNgayCapNhat(new Date(orderRowData.ngayCapNhat));
            setId(orderRowData.id);
            trangThaiArr.includes(orderRowData.trangThai.toLowerCase()) ? setTrangThai(orderRowData.trangThai.toLowerCase()) : setTrangThai("none");
        }
    }, [orderRowData])

    const onSubmitHandle = (event) => {
        event.preventDefault();
        if (trangThai === "" || trangThai === "none") {
            snackbarSeverity("error");
            snackbarMess("Hãy chọn trạng thái đơn hàng!");
            snackbarOpen();
        }
        else {
            let body = {
                method: 'PUT',
                body: JSON.stringify({
                    trangThai: trangThai
                }),
                headers: {
                    'Content-type': 'application/json; charset=UTF-8',
                }
            };
            updateOrderApi("http://42.115.221.44:8080/devcamp-pizza365/orders/" + id, body)
                .then((data) => {
                    snackbarSeverity("success");
                    snackbarMess("Đã Cập nhật Order thành công!");
                    snackbarOpen();
                    closeModal();
                })
                .catch((error) => {
                    snackbarSeverity("error");
                    snackbarMess(error);
                    snackbarOpen();
                })
        }
    }

    return (
        <Modal open={openModal} onClose={closeModal} id="modal-update-user" aria-labelledby="modal-modal-title" aria-describedby="modal-modal-description" >
            <Box sx={style}>
                <Typography id="modal-modal-title" variant="h4" component="h2" textAlign="center" marginBottom={2}>
                    Order Detail!
                </Typography>
                <Divider />
                <Form onSubmit={onSubmitHandle}>
                    <Grid container spacing={1} marginTop={3}>
                        <Grid item xs={4}>
                            <Grid container spacing={2} marginBottom={3}>
                                <Grid item xs={4}>
                                    <label>Họ và tên:</label>
                                </Grid>
                                <Grid item xs={8}>
                                    <TextField variant="outlined" value={hoTen} disabled />
                                </Grid>
                            </Grid>
                            <Grid container spacing={2} marginBottom={3}>
                                <Grid item xs={4}>
                                    <label>Địa chỉ:</label>
                                </Grid>
                                <Grid item xs={8}>
                                    <TextField variant="outlined" value={diaChi} disabled />
                                </Grid>
                            </Grid>
                            <Grid container spacing={2} marginBottom={3}>
                                <Grid item xs={4}>
                                    <label>Số điện thoại:</label>
                                </Grid>
                                <Grid item xs={8}>
                                    <TextField variant="outlined" value={soDienThoai} disabled />
                                </Grid>
                            </Grid>
                            <Grid container spacing={2} marginBottom={3}>
                                <Grid item xs={4}>
                                    <label>Email:</label>
                                </Grid>
                                <Grid item xs={8}>
                                    <TextField variant="outlined" value={email} disabled />
                                </Grid>
                            </Grid>
                            <Grid container spacing={2} marginBottom={3}>
                                <Grid item xs={4}>
                                    <label>Lời nhắn:</label>
                                </Grid>
                                <Grid item xs={8}>
                                    <TextField variant="outlined" multiline value={loiNhan} disabled />
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xs={8}>
                            <Grid container spacing={1}>
                                <Grid item xs={6}>
                                    <Grid container spacing={2} marginBottom={3}>
                                        <Grid item xs={6}>
                                            <label>Mã đơn hàng:</label>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <TextField variant="outlined" value={orderId} disabled />
                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={2} marginBottom={3}>
                                        <Grid item xs={6}>
                                            <label>Cỡ combo:</label>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <TextField variant="outlined" value={kichCo} disabled />
                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={2} marginBottom={3}>
                                        <Grid item xs={6}>
                                            <label>Sườn nướng (miếng):</label>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <TextField variant="outlined" value={suon} disabled />
                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={2} marginBottom={3}>
                                        <Grid item xs={6}>
                                            <label>Loại Đồ uống:</label>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <TextField variant="outlined" value={idLoaiNuocUong} disabled />
                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={2} marginBottom={3}>
                                        <Grid item xs={6}>
                                            <label>Loại pizza:</label>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <TextField variant="outlined" value={loaiPizza} disabled />
                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={2} marginBottom={3}>
                                        <Grid item xs={6}>
                                            <label>Mã voucher:</label>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <TextField variant="outlined" value={idVourcher} disabled />
                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={2} marginBottom={3}>
                                        <Grid item xs={6}>
                                            <label>Ngày tạo đơn:</label>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <TextField variant="outlined" value={ngayTao} disabled />
                                        </Grid>
                                    </Grid>
                                </Grid>
                                <Grid item xs={6}>
                                    <Grid container spacing={2} marginBottom={3}>
                                        <Grid item xs={6}>
                                            <label>Trạng thái đơn hàng:</label>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Select fullWidth value={trangThai} onChange={(event) => { setTrangThai(event.target.value) }}>
                                                <MenuItem value="none">Chọn Trạng thái</MenuItem>
                                                <MenuItem value="open">Open</MenuItem>
                                                <MenuItem value="cancel">Đã hủy</MenuItem>
                                                <MenuItem value="confirmed">Đã xác nhận</MenuItem>
                                            </Select>
                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={2} marginBottom={3}>
                                        <Grid item xs={6}>
                                            <label>Đường kính pizza:</label>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <TextField variant="outlined" value={duongKinh} disabled />
                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={2} marginBottom={3}>
                                        <Grid item xs={6}>
                                            <label>Salad (g):</label>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <TextField variant="outlined" value={salad} disabled />
                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={2} marginBottom={3}>
                                        <Grid item xs={6}>
                                            <label>Số lượng đồ uống:</label>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <TextField variant="outlined" value={soLuongNuoc} disabled />
                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={2} marginBottom={3}>
                                        <Grid item xs={6}>
                                            <label>Giá combo:</label>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <TextField variant="outlined" value={thanhTien.toLocaleString('vi-VN', { style: 'currency', currency: 'VND' })} disabled />
                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={2} marginBottom={3}>
                                        <Grid item xs={6}>
                                            <label>Giảm giá:</label>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <TextField variant="outlined" value={giamGia.toLocaleString('vi-VN', { style: 'currency', currency: 'VND' })} disabled />
                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={2} marginBottom={3}>
                                        <Grid item xs={6}>
                                            <label>Ngày cập nhật:</label>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <TextField variant="outlined" value={ngayCapNhat} disabled />
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Divider />
                    <Grid container spacing={2} marginTop={3} justifyContent="space-around">
                        <Button type="submit" variant="contained" color="success">Update</Button>
                        <Button variant="outlined" onClick={closeModal}>Cancel</Button>
                    </Grid>
                </Form>
            </Box>
        </Modal>
    )
}
export default ModalUpdateOrder;