import { Pagination } from "@mui/material";
import { useState } from "react";

const PaginationComponent = ({countPage,onChangePageProp}) => {
    const [page, setPage] = useState(1);
    const changePageHandle = (event, value) => {
        setPage(value);
        onChangePageProp(value);
    }
    return (
        <>
            <Pagination count={countPage} defaultPage={page} color="secondary" onChange={changePageHandle}></Pagination>
        </>
    )
}
export default PaginationComponent;
