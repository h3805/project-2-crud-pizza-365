import { MenuItem, Select } from "@mui/material";
import { useState } from "react";

const SelectNumberPage = ({showPage,selectShowPageHandleProp}) => {
    const [page, setPage] = useState(showPage);
    const handleChange = (event) => {
        setPage(event.target.value);
        selectShowPageHandleProp(event.target.value);
    };
    return (
        <>
            <span>Show <Select value={page} onChange={handleChange} displayEmpty>
                <MenuItem value={5}>5</MenuItem>
                <MenuItem value={10}>10</MenuItem>
                <MenuItem value={25}>25</MenuItem>
                <MenuItem value={50}>50</MenuItem>
            </Select>  entries</span>
        </>
    )
}

export default SelectNumberPage;
