import Pizza365Manager from "./components/Pizza365Manager";

function App() {
  return (
    <div >
      <Pizza365Manager />
    </div>
  );
}

export default App;
